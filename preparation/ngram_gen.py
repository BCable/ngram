#!/usr/bin/env python3

import csv, gzip, os, re, sys
from copy import deepcopy


class ngrams(object):
	dictionary = None
	stopwords = None
	ngram_minlen = None
	freq_cutoff = None

	def __init__(self, ngram_minlen, ngram_maxlen, freq_cutoff):
		self.ngram_minlen = ngram_minlen
		self.ngram_maxlen = ngram_maxlen
		self.freq_cutoff = freq_cutoff

	def read_dict(self, dict_file):
		dict_fd = open(dict_file, "r")
		csv_lines = csv.reader(dict_fd)

		dictionary = {}
		first = True
		for line in csv_lines:
			# ignore header line
			if first:
				first = False
				continue

			dictionary[line[1]] = int(line[0])

		dict_fd.close()
		self.dictionary = dictionary

	def read_stopwords(self, stopwords_file):
		stop_fd = open(stopwords_file, "r")

		stopwords = []
		for line in stop_fd:
			line = line.strip()
			if line in self.dictionary:
				stopwords.append(self.dictionary[line])

		self.stopwords = stopwords

	def clean_word(self, word):
		stopword = False
		termword = False

		cmp_word = word
		word = re.sub(b"^[^a-z#@]", b"", word)

		if cmp_word != word:
			termword = True

		if len(word) == 0:
			return (True, None)

		if word[0] in (b"#", b"@"):
			termword = True

		cmp_word = word
		word = re.sub(b"[^a-z]$", b"", word)

		if len(word) == 0:
			return (True, None)

		if cmp_word != word:
			stopword = True

		word = re.sub(
			b"[^a-z]+$", b"",
			re.sub(b"^[^a-z]+", b"",
				re.sub(b"[^a-z']", b"", word)
			)
		).decode()

		if termword or len(word) == 0:
			return (True, None)
		else:
			return (stopword, word)

	def ngram_accept(self, ngram):
		return (
			len(ngram) >= self.ngram_minlen and len(ngram) <= self.ngram_maxlen
		)

	def iterate_ngrams(self, ngram):
		if len(ngram) < self.ngram_minlen:
			return []

		ret = []

		for ngram_len in range(self.ngram_minlen, self.ngram_maxlen+1):
			for i in range(0, len(ngram)-self.ngram_minlen):
				new_ngram = deepcopy(ngram[i:i+ngram_len])

				while len(new_ngram) != 0 and new_ngram[-1] < self.freq_cutoff:
					new_ngram = new_ngram[0:-1]

				while len(new_ngram) != 0 and new_ngram[0] == 0:
					new_ngram = new_ngram[1:]

				if not self.ngram_accept(new_ngram):
					continue

				# no ngrams with more than two "0" values and
				# at least 2 real words
				if (
					new_ngram.count(0) <= 2 and
					len(new_ngram)-new_ngram.count(0) >= 2
				):
					ret.append(new_ngram)

		return ret

	def parse_line(self, line):
		line = re.sub(
			b"[ ]+ ", b" ",
			re.sub(
				b"([a-z])[ ]+ ", b"\\1. ",
				line.strip().lower()
			)
		)

		words = line.split(b" ")
		end = False
		cur_ngram = []
		ngrams = []
		stopword = False
		last_stopword = False

		for word in words:
			stopword, word = self.clean_word(word)

			if word is not None:
				if word not in self.dictionary:
					word_num = 0
				else:
					word_num = self.dictionary[word]

				cur_ngram.append(word_num)

			last_stopword = stopword

			if stopword:
				stopword = False
				ngrams.extend(self.iterate_ngrams(cur_ngram))
				cur_ngram = []

		if not last_stopword:
			ngrams.extend(self.iterate_ngrams(cur_ngram))

		return ngrams

	def loop_lines(self, src_fd, dest_file):
		dst_fd = open(dest_file, "wb")

		i = 0
		for line in src_fd:
			i += 1

			ngrams = self.parse_line(line)
			for ngram in ngrams:
				bytes_list = [str(word_num) for word_num in ngram]
				string_concat = ",".join(bytes_list)
				dst_fd.write(string_concat.encode("ascii"))
				dst_fd.write(b"\n")

			if i%100000 == 0:
				print("Status: {}".format(i))

		dst_fd.close()


if __name__ == "__main__":
	if len(sys.argv) < 6:
		print("Not enough arguments.")
		print(
			"Usage: {} ".format(sys.argv[0]) +
			"<source_file> <dest_file> <dict_file> <stopwords_file> " +
			"<ngram_minlen> <ngram_maxlen>"
		)
		sys.exit(1)

	if not os.path.exists(sys.argv[1]):
		print("Source file not found.")
		sys.exit(2)

	if os.path.exists(sys.argv[2]):
		print("Destination file exists.")
		sys.exit(3)

	if sys.argv[2] == "":
		print("Empty destination filename.")
		sys.exit(4)

	if not os.path.exists(sys.argv[3]):
		print("Dictionary file not found.")
		sys.exit(5)

	#if not os.path.exists(sys.argv[4]):
		#print("Stopwords file not found.")
		#sys.exit(6)

	if not sys.argv[4].isdigit():
		print("Invalid frequency cutoff.")
		sys.exit(6)

	freq_cutoff = int(sys.argv[4])

	if not sys.argv[5].isdigit():
		print("Invalid ngram format.")
		sys.exit(7)

	ngram_minlen = int(sys.argv[5])

	if ngram_minlen < 3 or ngram_minlen > 6:
		print("Invalid ngram length.")
		sys.exit(8)

	ngram_maxlen = int(sys.argv[6])

	if ngram_maxlen < 3 or ngram_maxlen > 6:
		print("Invalid ngram length.")
		sys.exit(9)

	call_obj = ngrams(ngram_minlen, ngram_maxlen, freq_cutoff)
	call_obj.read_dict(sys.argv[3])
	#call_obj.read_stopwords(sys.argv[4])

	fd = gzip.open(sys.argv[1], "rb")
	call_obj.loop_lines(fd, sys.argv[2])
	fd.close()
