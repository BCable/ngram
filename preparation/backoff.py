#!/usr/bin/env python3

import csv, math, numpy, os, pickle, sys

from copy import deepcopy

numpy.seterr(all='raise')

class katz(object):
	k = 0
	_counts = None

	def __init__(self):
		self._counts = {}

	def load(self, filename):
		fd = gzip.open(filename, "rb")
		for line in fd:
			words, count = line.split(",")
			words_list = words.split("_")
			predict_word = words_list[-1]
			store_words = "_".join(words_list[0:-1])
			self._counts[store_words] = (predict, int(count))
		fd.close()

	# http://www.grsampson.net/AGtf1.html
	def simple_good_turing(self, words):
		# preprocess, generate a workable table to look up with
		ret = self.traverse(words)
		if ret[2] is False:
			return {}

		rr_words = sorted(ret[2], key=lambda x: ret[2][x][0])
		rr_vals = [ret[2][x][0] for x in rr_words]

		print("TEST2", rr_words, rr_vals)

		last_r = 0
		cur_nr = 0
		nr_vals = []
		r_vals = []
		for r_val in rr_vals:
			if last_r == r_val:
				cur_nr += 1
			else:
				if cur_nr != 0:
					r_vals.append(last_r)
					nr_vals.append(cur_nr)
				cur_nr = 1

			last_r = r_val

		r_vals.append(last_r)
		nr_vals.append(cur_nr)

		print("TESTING", nr_vals, r_vals)

		### generate z values
		##z_vals = []
		##for j in range(0, len(nr_vals)):
		##	j -= 1
		##	i = j - 1
		##	if j == len(nr_vals)-2:
		##		k = 2*j - i
		##	else:
		##		k = i + 1

		##	z_vals.append((2*nr_vals[j])/(k-i))

		##print("ZVAL", z_vals)

		# p0 calculation
		if r_vals[0] == 1:
			p0 = nr_vals[0]/ret[0]
		else:
			p0 = 1/ret[0]

		## log vals
		#log_r_vals = []
		##log_z_vals = []
		#for i in range(0, len(nr_vals)):
		#	log_r_vals.append(math.log(r_vals[i], 10))
		#	#log_z_vals.append(math.log(z_vals[i], 10))

		# linear regression
		A = numpy.vstack([r_vals, numpy.ones(len(r_vals))]).T
		m,c = numpy.linalg.lstsq(A, nr_vals)[0]

		print("LINE", m, c)

		## r* calculation
		#r_star = []
		#for i in range(0, len(r_vals)):
		#	numer = (r_vals[i]+1) * nr_vals[i+1]
		#	denom = nr_vals[i]
		#	x = numer/denom

		#	numer = (r_vals[i]+1) * 
		#	denom = nr_vals[i]
		#	y = numer/denom

		## N' calculation
		#for word in r_star:
		#	nr_vals[i]

		# final sgt calculation
		sgt = {}
		for word in ret[2]:
			word_count = self.count(words + [word])
			idx = r_vals.index(word_count)

			sgt[word] = (m*r_vals[idx]+c) * ret[2][word][0]

		return sgt

	def delta(self, words):
		sgt = self.simple_good_turing(words)
		delta_ret = {}

		for word in sgt:
			delta_ret[word] = (sgt[word] / self.count(words + [word])) / (
				self.count(words + [word]) / self.count(words)
			)

		return delta_ret

	def kneser_ney(self, word):
		if word in self.counts:
			return self.counts[word]/self.counts[word]
		else:
			return 1

	def count(self, words, check_subwords=False):
		ret = self.traverse(words)

		if ret is None:
			return 0
		elif check_subwords and ret[2] is False:
			return 0
		else:
			return ret[0]

	def traverse(self, words, trav_obj=None):
		search_pattern = "_".join(words) + "_"

		if trav_obj is None:
			trav_obj = self.paths

		if trav_obj is False:
			return [0, 0, False]

		elif words[0] in trav_obj:
			if len(words) == 1:
				return trav_obj[words[0]]
			else:
				return self.traverse(words[1:], trav_obj[words[0]][2])

		else:
			return [0, 0, False]

	def predict_one(self, words, alpha=1):
		words_count = self.count(words, True)
		return self.simple_good_turing(words)

		deltas = self.delta(words)
		print("DELTAS", deltas)
		return {word: deltas[word] * alpha * (
			self.count(words + [word]) / words_count
		) for word in deltas}

	def predict(self, words, empty_words=0, alpha=1):
		if empty_words > 0:
			words_count = self.count(words, True)
			print("WORDS_COUNT: {} :: {}".format(words_count, words))

			results = {}

			if words_count > self.k:
				results = self.predict_one(words, alpha=alpha)
				print("FIRST", results)
				return results

		elif empty_words > 2:
			return {}

		for i in range(0, len(words)):
			if words[i] == 0:
				continue

			sub_words = deepcopy(words)
			sub_words[i] = 0
			ret = self.predict(sub_words, empty_words+1, alpha*0.75)

			# update
			for result in ret:
				if result not in results:
					results[result] = ret[result]
					continue

				if results[result] > ret[result]:
					results[result] = ret[result]

		print("FINAL", results)
		return results

	def predict_file(self, predict_file):
		fd = open(predict_file, "r")
		cfd = csv.reader(fd)

		ret = []
		for words in cfd:
			words = [int(word) for word in words]

			highest_predict = 0
			for i in range(0, len(words)):
				predictions = self.predict(words)
				if predictions is None or len(predictions) == 0:
					continue

				else:
					high_predict = max(
						predictions, key=lambda x: predictions[x]
					)
					high_value = predictions[high_predict]
					print("HIGH PREDICT: {}:: {}".format(
						high_predict,
						high_value
					))

				if high_value > highest_value:
					highest_predict = high_predict
					highest_value = high_value

			if predictions is None or len(predictions) == 0:
				ret.append(False)
			else:
				ret.append(highest_predict)

		fd.close()
		return ret


if __name__ == "__main__":
	if len(sys.argv) < 3:
		print("Not enough arguments.")
		sys.exit(1)

	if not os.path.exists(sys.argv[1]):
		print("Model not found")
		sys.exit(2)

	if not os.path.exists(sys.argv[2]):
		print("Predictive data not found.")
		sys.exit(3)

	model = backoff()
	model.load(sys.argv[1])
	print(model.predict_file(sys.argv[2]))
