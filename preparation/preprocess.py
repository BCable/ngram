#!/usr/bin/env python3

import csv, gzip, os, sys
from copy import deepcopy


class preprocess(object):
	_counts = None
	_strip_cutoff = 2
	_output_file = None

	def __init__(self, output_file=None):
		self._counts = {}
		if output_file is not None:
			self._output_file = open(output_file, "wb")

	def counts_inc(self, words):
		# no ngrams with more than two "0" values and at least 2 real words
		if words.count(0) > 2 or len(words)-words.count(0) < 2:
			return

		idx = "_".join([str(word) for word in words])

		if idx in self._counts:
			self._counts[idx] += 1
		else:
			self._counts[idx] = 1

	def counts_store(self, words):
		# no ngrams with more than two "0" values and at least 2 real words
		if words.count(0) > 2 or len(words)-words.count(0) < 2:
			return

		self._output_file.write(
			(",".join([str(word) for word in words]) + "\n").encode()
		)

	def counts_filter(self, words):
		for key in self._counts:
			if self._counts[key] <= self._strip_cutoff:
				del self._counts[key]

	def gen_predata(self, train_file):
		fd = open(train_file, "r")
		cfd = csv.reader(fd)

		cnt = 1
		for words in cfd:
			words = [int(word) for word in words]

			if cnt%100000 == 0:
				print("TRAIN:", int(cnt/100000), words)

			tried = []
			for i in range(1, len(words)-1):
				inew_words = deepcopy(words)
				inew_words[i] = 0
				tried.append(inew_words)
				#self.counts_inc(inew_words)
				self.counts_store(inew_words)

				for j in range(1, len(words)-1):
					if i == j:
						continue

					jnew_words = deepcopy(inew_words)
					jnew_words[j] = 0

					# make sure we don't count repeated generates more than once
					if jnew_words not in tried:
						self.counts_store(jnew_words)
						#self.counts_inc(jnew_words)
						tried.append(jnew_words)

			cnt += 1

		fd.close()

	def write_model(self, filename):
		fd = gzip.open(filename, "wb")
		for idx in self._counts:
			if self._counts[idx] > self._strip_cutoff:
				fd.write("{},{}\n".format(idx, self._counts[idx]).encode())
		fd.close()

	def close(self):
		self._output_file.close()
		print("CLOSED")


if __name__ == "__main__":
	if len(sys.argv) < 3:
		print("Not enough arguments.")
		sys.exit(1)

	if os.path.exists(sys.argv[1]):
		print("Output file exists.")
		sys.exit(2)

	pre_obj = preprocess(sys.argv[1])

	for i in range(2, len(sys.argv)):
		if not os.path.exists(sys.argv[2]):
			print("Training data not found.")
			sys.exit(2)

		pre_obj.gen_predata(sys.argv[i])

	pre_obj.close()

	#pre_obj.counts_filter()
	#pre_obj.write_model(sys.argv[1])
