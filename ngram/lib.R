dict_lookup <- function(word_num){
	as.character(dictDF[dictDF$ID == word_num,]$Word)
}

revdict_lookup <- function(word){
	dictDF[dictDF$Word == word,]$ID
}

lookup_list <- function(thevector, FUN){
	ret <- NULL
	for(i in 1:length(thevector)){
		ret <- c(ret, FUN(thevector[[i]]))
	}
	ret
}

dict_lookup_list <- function(words_vector){
	lookup_list(words_vector, dict_lookup)
}

revdict_lookup_list <- function(words_vector){
	lookup_list(words_vector, revdict_lookup)
}

expand_ngram <- function(ngram){
	new_ngram <- ngram
	while(length(new_ngram) < 5){
		new_ngram <- c(0, new_ngram)
	}
	new_ngram
}

grab_counts <- function(ngram, predictBlacklist=NULL){
	if(class(predictBlacklist) != "NULL"){
		modelDF[
			modelDF$N1 == ngram[1] &
			modelDF$N2 == ngram[2] &
			modelDF$N3 == ngram[3] &
			modelDF$N4 == ngram[4] &
			modelDF$N5 == ngram[5] &
			!(modelDF$Prediction %in% predictBlacklist)
		,]
	} else {
		modelDF[
			modelDF$N1 == ngram[1] &
			modelDF$N2 == ngram[2] &
			modelDF$N3 == ngram[3] &
			modelDF$N4 == ngram[4] &
			modelDF$N5 == ngram[5]
		,]
	}
}

grab_all_counts <- function(ngram, predictBlacklist=NULL){
	orig_len <- length(ngram)
	ngram <- expand_ngram(ngram)
	start <- 6-orig_len

	ret <- NULL

	for(i in start:5){
		new_ngram <- ngram
		removed_wordfreq <- ngram[i]
		removed_wordlen <- nchar(dict_lookup(ngram[i]))
		new_ngram[i] <- 0
		cur_ret <- grab_counts(new_ngram, predictBlacklist)
		if(class(ret) == "NULL"){
			ret <- cur_ret
		} else {
			ret <- rbind(ret, cur_ret)
		}
	}

	if(nrow(ret) == 0 || class(ret) == "NULL"){
		data.frame(Prediction=1, Score=1)[FALSE,]
	} else {
		aggDF <- data.frame(
			Prediction=ret$Prediction,
			Score=ret$Count
		)
		aggDF <- aggregate(. ~ Prediction, data=aggDF, FUN=prod)
		aggDF$Score <- aggDF$Score / sum(aggDF$Score, na.rm=TRUE)
		aggDF
	}
}

get_phrase_vector <- function(phrase, ng=5){
	clean_phrase <- gsub("[^a-z' ]", "", tolower(phrase))
	phrase_vector <- revdict_lookup_list(
		strsplit(clean_phrase, " ")[[1]]
	)
	if(length(phrase_vector) > ng){
		phrase_vector <- phrase_vector[
			seq(length(phrase_vector)-(ng-1), length(phrase_vector))
		]
	}
	phrase_vector
}

predict_phrase <- function(phrase, ng=5, predictBlacklist=NULL){
	phrase_vector <- get_phrase_vector(phrase, ng)

	predictDF <- grab_all_counts(
		phrase_vector, predictBlacklist=predictBlacklist
	)

	predictDF
}

head_predict_phrase <- function(phrase, n=3){
	ng <- 5
	predictions <- NULL
	while(length(predictions) < n && ng > 1){
		predictRet <- predict_phrase(phrase, ng, predictBlacklist=predictions)
		predictions <- c(
			predictions, predictRet[
				order(-predictRet$Score, predictRet$Prediction),
			]$Prediction
		)
		ng <- ng - 1
	}
	as.character(head(dictDF[predictions,]$Word, n))
}
