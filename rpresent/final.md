N-Gram Aggregate Prediction
========================================================
author: Brad Cable
date: 2016-01-23

Model Generation Algorithm
========================================================

<style>
p {
  font-size: 25px !important;
  margin: 10px 0px 0px 0px !important;
}
ul {
  padding: 0px 0px 0px 50px !important;
  list-style-type: circle !important;
}
ul, ul li {
  font-size: 15px !important;
  font-family: bitstream mono sans, monospace !important;
  margin: 0px !important;
  line-height: 1.5 !important;
}
span.ngram {
  font-family: bitstream mono sans, monospace;
  font-size: 20px !important;
}
</style>

By removing one word from each n-gram stored, in theory the predictive value of the data will not be removed if the right algorithm is applied to the resulting model.  A cutoff at a certain threshold of counts strips many meaningless entries.  For instance:

"<span class="ngram">i want you</span>" becomes:
 - "i _ you" + 1 (total 1 so far)
 - "i want _" + 1 (total 1 so far)
 - "want _" + 1 (total 1 so far)
 
"<span class="ngram">i need you</span>" becomes:
 - "i _ you" + 1 (total 2 so far)
 - "i need _" + 1 (total 1 so far)
 - "need _" + 1 (total 1 so far)
 
"<span class="ngram">i need coffee</span>" becomes:
 - "I _ coffee" + 1 (total 1 so far)
 - "I need _ " + 1 (total 2 so far)
 - "need _" + 1 (total 2 so far)

After a threshold is applied, this can become:
 - "I want _" = 2
 - "I need _" = 2
 - "need _" = 2


Prediction Algorithm
========================================================

After generating the model, a modified Katz backoff algorithm is used.

Because Katz requires exact n-gram matches, the algorithm had to be adjusted slightly.  By searching for semi-matches instead of exact matches, the countscan be generated just the same.  For each n-gram, such as "<span class="ngram">I love your hair</span>", searching is done on "<span class="ngram">I _ your</span>" and "<span class="ngram">I love _</span>", and the counts for each possible predicted word are combined by taking the square root of the ratios of the counts for the prediction itself to the sum of all counts in the individual result.  This produces a combined ratio for a score.

From here, a backoff is performed and searches for "<span class="ngram">love \_</span>" and "<span class="ngram">_ your</span>".  These are also combined as ratios to produce a final result.



Data Product
========================================================

the aggregate data can use as many n-grams as desired while not creating a large overhead.  Since the idea is that this product would be accurate and possible to be used on a smart phone or tablet, optimization of the largest quantity of data while minimizing the CPU/RAM overhead was what I focused on.

The data model produced is a 13MB gzip file which contains aggregations of 157,131,910 n-grams.  This allows for a lot of information to be essentially compressed through the aggregation while maintaining high quantities of information.


Limitations and Future Improvements
========================================================

2-grams and 3-grams don't predict very well.  The reason for this is that when removing individual words with so few words to begin with, much of the predictive quality is lost.

As a result, the higher n-grams do remarkably better, but sometimes are completely incorrect.

This can be fixed by inserting non-aggregated 2-grams and 3-grams to provide more solid data on lower n-grams, and on higher n-grams this will improve the quality of the ratios in the backoff models to prevent having as much off-topic higher n-grams.
